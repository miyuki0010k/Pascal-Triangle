# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.26

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /mnt/SSD/Programs/PascalTriangle

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /mnt/SSD/Programs/PascalTriangle/build

# Include any dependencies generated for this target.
include CMakeFiles/PascalTriangle.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/PascalTriangle.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/PascalTriangle.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/PascalTriangle.dir/flags.make

CMakeFiles/PascalTriangle.dir/main.cpp.o: CMakeFiles/PascalTriangle.dir/flags.make
CMakeFiles/PascalTriangle.dir/main.cpp.o: /mnt/SSD/Programs/PascalTriangle/main.cpp
CMakeFiles/PascalTriangle.dir/main.cpp.o: CMakeFiles/PascalTriangle.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/mnt/SSD/Programs/PascalTriangle/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/PascalTriangle.dir/main.cpp.o"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/PascalTriangle.dir/main.cpp.o -MF CMakeFiles/PascalTriangle.dir/main.cpp.o.d -o CMakeFiles/PascalTriangle.dir/main.cpp.o -c /mnt/SSD/Programs/PascalTriangle/main.cpp

CMakeFiles/PascalTriangle.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/PascalTriangle.dir/main.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /mnt/SSD/Programs/PascalTriangle/main.cpp > CMakeFiles/PascalTriangle.dir/main.cpp.i

CMakeFiles/PascalTriangle.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/PascalTriangle.dir/main.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /mnt/SSD/Programs/PascalTriangle/main.cpp -o CMakeFiles/PascalTriangle.dir/main.cpp.s

# Object files for target PascalTriangle
PascalTriangle_OBJECTS = \
"CMakeFiles/PascalTriangle.dir/main.cpp.o"

# External object files for target PascalTriangle
PascalTriangle_EXTERNAL_OBJECTS =

PascalTriangle: CMakeFiles/PascalTriangle.dir/main.cpp.o
PascalTriangle: CMakeFiles/PascalTriangle.dir/build.make
PascalTriangle: CMakeFiles/PascalTriangle.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/mnt/SSD/Programs/PascalTriangle/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable PascalTriangle"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/PascalTriangle.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/PascalTriangle.dir/build: PascalTriangle
.PHONY : CMakeFiles/PascalTriangle.dir/build

CMakeFiles/PascalTriangle.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/PascalTriangle.dir/cmake_clean.cmake
.PHONY : CMakeFiles/PascalTriangle.dir/clean

CMakeFiles/PascalTriangle.dir/depend:
	cd /mnt/SSD/Programs/PascalTriangle/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /mnt/SSD/Programs/PascalTriangle /mnt/SSD/Programs/PascalTriangle /mnt/SSD/Programs/PascalTriangle/build /mnt/SSD/Programs/PascalTriangle/build /mnt/SSD/Programs/PascalTriangle/build/CMakeFiles/PascalTriangle.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/PascalTriangle.dir/depend

