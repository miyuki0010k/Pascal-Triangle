// "Copyright [2023] miyuki0010k"

#include <algorithm>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std ::max;
using std::setw;
using std::string;
using std::vector;

void printTriangle(int);
int64_t binomialCoeff(int, int);
char getSymbol(int64_t);

int main() {
  int n;
  cout << "Input the depth of the trianle: ";
  cin >> n;

  printTriangle(n);

  return 0;
}

void printTriangle(int n) {
  // calcuate center
  int center = n / 2;

  for (int i = 0; i < n; ++i) {
    int padding = (n - i) / 2;

    int maxSymWidth{1};
    for (int i{center}; i < n; ++i) {
      int64_t coeff = binomialCoeff(i, center);
      char symbol = getSymbol(coeff);
      int symWidth = static_cast<int>(symbol != ' ' ? 1 : 3);
      maxSymWidth = max(maxSymWidth, symWidth);
    }

    cout << string(padding * maxSymWidth, ' ');

    for (int j = 0; j < i; ++j) {
      int64_t coeff = binomialCoeff(i, j);
      if (coeff % 2 == 1) {
        char symbol = (i >= n / 2) ? getSymbol(coeff) : '1';
        cout << "\033[1;31m" << setw(maxSymWidth) << symbol << "\033[0m ";
      } else {
        cout << setw(maxSymWidth + 1) << " ";
      }
    }
    cout << string(padding * maxSymWidth, ' ') << endl;
  }
}
int64_t binomialCoeff(int n, int k) {
  int64_t res = 1;

  if (k > n - k)
    k = n - k;

  for (int i = 0; i < k; ++i) {
    res *= (n - i);
    res /= (i + 1);
  }
  return res;
}

char getSymbol(int64_t n) {
  const vector<char> symbols = {'@', '#', '$', '%', '&'};
  int symbolIndex = n % symbols.size();
  return symbols[symbolIndex];
}
